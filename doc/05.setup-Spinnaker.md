# Spinnaker を使った継続的デリバリ (CD) に対応したデプロイ

- Istioでサービスメッシュを構築してからデプロイを実施しますが、Istioをインストールした後にSpinnakerをインストールすると失敗する事が多いです
  - BackoffLimitExceeded のエラーになる
    - インストールに失敗した場合、クラスタを削除してから再度実行することになる
- 本ハンズオンでは、Spinnakerをインストールしてから、Istioをインストールし、SpinnakerでCDを実行する手順で環境を構築します


## ID およびアクセス管理を構成する
- Cloud Identity Access Management（Cloud IAM）サービス アカウントを作成して Spinnaker に権限を委任し、Cloud Storage にデータを保存できるようにします。

- サービスアカウントを生成

```
$ gcloud iam service-accounts create spinnaker-account \
    --display-name spinnaker-account
```

- IAM のUIに spinnaker-account が生成されている
  - IAMと管理 > サービスアカウント
    - 「名前」が `spinnaker-account` になっているアカウントが追加されている

- 後のコマンドで使用するために、サービス アカウントのメールアドレスと現在のプロジェクト ID を環境変数に格納します。

```
$ export SA_EMAIL=$(gcloud iam service-accounts list \
    --filter="displayName:spinnaker-account" \
    --format='value(email)') && \
  export PROJECT=$(gcloud info --format='value(config.project)') && \
  echo $SA_EMAIL $PROJECT
```

- storage.admin 役割をサービス アカウントにバインドします。

```
$ gcloud projects add-iam-policy-binding \
    $PROJECT --role roles/storage.admin --member serviceAccount:$SA_EMAIL
```

- サービス アカウントキーをホームディレクトリにダウンロードしておきます
  - 後のステップで Spinnaker をインストールして、このキーを Kubernetes Engine にアップロードします

```
$ cd && \
  gcloud iam service-accounts keys create spinnaker-sa.json --iam-account $SA_EMAIL
```


## Spinnaker パイプラインをトリガーする Cloud Pub/Sub を設定する

- Container Registry からの通知に使用する Cloud Pub/Sub トピックを作成します
  - データ量は、pull、push、パブリッシュの各オペレーションのメッセージ データと属性データを使用して計算されます。
  - 最初の 10 GB まで無料

```
$ gcloud pubsub topics create projects/$PROJECT/topics/gcr
```

- GUI の Pub/Subに `gcr` のトピックが追加されていることを確認

- イメージの push についての通知を受け取れるように、Spinnaker から読み取ることができるサブスクリプションを作成します。

```
$ gcloud pubsub subscriptions create gcr-triggers \
    --topic projects/${PROJECT}/topics/gcr
```

- Spinnakerサービスアカウントにgcr-triggers subscriptionを使用するの許可を与える。
  - `gcloud pubsub beta subscriptions add-iam-policy-binding gcr-triggers` のコマンドは `beta` なので注意

```
$ export SA_EMAIL=$(gcloud iam service-accounts list \
    --filter="displayName:spinnaker-account" \
    --format='value(email)') && \
  gcloud beta pubsub subscriptions add-iam-policy-binding gcr-triggers \
    --role roles/pubsub.subscriber --member serviceAccount:$SA_EMAIL
```

## Helm を使用した Spinnaker のデプロイ

### Helm をインストールする

- Helm バイナリをホームディレクトリにダウンロード

```
$ cd && \
  wget https://storage.googleapis.com/kubernetes-helm/helm-v2.10.0-linux-amd64.tar.gz
```

- ファイルをローカル システムに解凍

```
$ tar zxfv helm-v2.10.0-linux-amd64.tar.gz && \
  cp linux-amd64/helm .
```

- クラスタ内のHelm のサーバー側である Tillerにcluster-adminロールを付与します。

```
$ kubectl create clusterrolebinding user-admin-binding --clusterrole=cluster-admin \
      --user=$(gcloud config get-value account) && \
  kubectl create serviceaccount tiller --namespace kube-system && \
  kubectl create clusterrolebinding tiller-admin-binding --clusterrole=cluster-admin \
      --serviceaccount=kube-system:tiller
```

- Spinnaker に cluster-admin 役割を付与し、すべての名前空間にリソースをデプロイできるようにします。

```
$ kubectl create clusterrolebinding --clusterrole=cluster-admin \
    --serviceaccount=default:default spinnaker-admin
```

- Helm を初期化して、クラスタに Tiller をインストールします

```
$ ./helm init --service-account=tiller && \
  ./helm update && \
  sleep 10 && \
  ./helm version
```


### Spinnaker を構成する

- Spinnaker でパイプライン構成を保存するためのバケットを作成します。

```
$ export PROJECT=$(gcloud info --format='value(config.project)')  && \
  export BUCKET=$PROJECT-spinnaker-config && \
  gsutil mb -c regional -l us-central1 gs://$BUCKET
```

- Spinnaker のインストール方法の構成を記述するファイル（spinnaker-config.yaml）を作成します。

```
$ export SA_JSON=$(cat spinnaker-sa.json) && \
  export PROJECT=$(gcloud info --format='value(config.project)') && \
  export BUCKET=$PROJECT-spinnaker-config
```

```
$ cat > spinnaker-config.yaml <<EOF
gcs:
  enabled: true
  bucket: $BUCKET
  project: $PROJECT
  jsonKey: '$SA_JSON'

dockerRegistries:
- name: gcr
  address: https://gcr.io
  username: _json_key
  password: '$SA_JSON'
  email: 1234@5678.com

# Disable minio as the default storage backend
minio:
  enabled: false

# Configure Spinnaker to enable GCP services
halyard:
  spinnakerVersion: 1.10.2
  image:
    tag: 1.12.0
  additionalScripts:
    create: true
    data:
      enable_gcs_artifacts.sh: |-
        \$HAL_COMMAND config artifact gcs account add gcs-$PROJECT --json-path /opt/gcs/key.json
        \$HAL_COMMAND config artifact gcs enable
      enable_pubsub_triggers.sh: |-
        \$HAL_COMMAND config pubsub google enable
        \$HAL_COMMAND config pubsub google subscription add gcr-triggers \
          --subscription-name gcr-triggers \
          --json-path /opt/gcs/key.json \
          --project $PROJECT \
          --message-format GCR
EOF
```

### Spinnaker チャートをデプロイする

- Helm コマンドライン インターフェースを使用して、構成セットとともにチャートをデプロイします。
- このコマンドは完了するまでに 5〜10 分程度かかります。

```
$ ./helm install -n cd stable/spinnaker -f spinnaker-config.yaml \
    --timeout 9000 --version 1.1.6 --wait
```

- ブラウザでGKEの状況を確認できる（新しいサービスがデプロイされていく）

<img src="image/deploy-spinnaker.png" width="70%" />



- pod の起動を待つ
  - Pending ではなくなるまで

```
$ kubectl get pod
```

- コマンドが完了したら、次のコマンドを実行して、Cloud Shell から Spinnaker UI へのポート転送を設定します

```
$ export DECK_POD=$(kubectl get pods --namespace default -l "cluster=spin-deck" \
      -o jsonpath="{.items[0].metadata.name}") && \
   kubectl port-forward --namespace default $DECK_POD 8080:9000 >> /dev/null &
```


- Spinnaker ユーザー インターフェースを開くには、Cloud Shell ウィンドウの最上部で [ウェブでプレビュー] をクリックし、[プレビューのポート: 8080] をクリックします。
  - ようこそ画面が表示されてから、Spinnaker UI が表示されます。

- Cloud Shell ウィンドウ上部に[ウェブでプレビュー]がある
<img src="image/preview-spinnaker.png" width="70%" />

- プレビューすると、Spinnakerにアクセスできる
<img src="image/splash-spinnaker.png" width="30%" />
<img src="image/hello-spinnaker.png" width="70%" />


## クリーンアップ

- 次のセッションは、このままの環境を使って進めるので、この章ではクリーンアップを行いません


**おつかれさまでした。次のセクションに進みます。**

