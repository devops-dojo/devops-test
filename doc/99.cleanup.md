# クリーンアップ

- ハンズオンで利用したものを、いろいろ削除していく
- 最後に、GCPのプロジェクトを削除（←たぶん、これだけでも問題無い）

- Kubernetes Engine のクラスタを削除
- IAMと管理から、サービスアカウントを削除
- Source Repositories を削除
- Container Registry からイメージを削除
- Cloud Storageからバケットを削除
- GCPのプロジェクトを削除
  - GUIから削除する



**おつかれさまでした。ハンズオン終了です！**

